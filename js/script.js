/**
 * @author Lucas Vieira <lucas.engen.cc@gmail.com>
 */
(function ($) {
    "use strict";
    $(".sakura-falling").sakura();
})(jQuery);

window.onload = function () {
    var audio = document.getElementById("my_audio");
    audio.play().catch(function (error) {
        console.log("Autoplay bloqueado, o usuário precisa interagir: ", error);
    });
};

// Oculta a imagem inicial
let img_inicial = document.getElementById("img_casal");
img_inicial.style.display = "none";

// Set the date we're counting down to
var countDownDate = new Date("Oct 26, 2024 18:00:00").getTime();

function createDiaCasamento(root_element) {
    var novoP = document.createElement("p");
    novoP.className = "dia-casamento";

    // Define o conteúdo do novo elemento P
    novoP.innerHTML =
        '<span class="date">26 de Outubro de 2024</span> em <span class="place">IGREJA UNIVERSAL</span> EQRSW 6/7, 01 - Setor Sudoeste, Brasília - Distrito Federal';

    root_element.appendChild(novoP);
}

function removeAllDivElements() {
    let div_title = document.getElementById("div-titulo");

    // remove todos os elementos da div
    while (div_title.firstChild) {
        div_title.removeChild(div_title.firstChild);
    }

    return div_title;
}

// Faz a animação que exibe o texto "Para o nosso casamento"
setTimeout(() => {
    let div_title = removeAllDivElements();

    // Adiciona um novo elemento
    let h1_novo = document.createElement("h1");
    h1_novo.textContent = "Para o nosso casamento!";
    h1_novo.style.margin = "50% auto";
    h1_novo.style.width = "50%";

    div_title.appendChild(h1_novo);
}, 8000);

// Faz a animação que exibe os que estão se casando
setTimeout(() => {
    let div_title = removeAllDivElements();

    // Cria e insere o primeiro elemento H1
    var h1Deuzimar = document.createElement("h1");
    h1Deuzimar.id = "deuzimar";
    h1Deuzimar.textContent = "Deuzimar";
    div_title.appendChild(h1Deuzimar);

    // Cria e insere o elemento H2
    var h2Uniao = document.createElement("h2");
    h2Uniao.id = "uniao";
    h2Uniao.textContent = "&";
    div_title.appendChild(h2Uniao);

    // Cria e insere o segundo elemento H1
    var h1Milene = document.createElement("h1");
    h1Milene.id = "milene";
    h1Milene.textContent = "Milene";
    div_title.appendChild(h1Milene);

    img_inicial.style.display = "block";
}, 16000);

// Faz a animação que exibe a data e a hora
setTimeout(() => {
    let div_title = removeAllDivElements();

    // Adiciona um novo elemento
    let h1_novo = document.createElement("h1");
    h1_novo.textContent = "Data:";
    h1_novo.style.margin = "50% auto";
    h1_novo.style.width = "50%";

    div_title.appendChild(h1_novo);
    createDiaCasamento(div_title);
    img_inicial.style.display = "none";
}, 24000);

setTimeout(() => {
    let paragraph = document.createElement("h1");

    paragraph.style.margin = "50% auto";
    paragraph.style.width = "70%";
    paragraph.innerHTML = "Esteja conosco nesse momento tão especial!";

    removeAllDivElements();

    let divTitulo = document.getElementById("div-titulo");
    divTitulo.appendChild(paragraph);
}, 32000);

setTimeout(() => {
    let div_title = removeAllDivElements();

    // Adiciona um novo elemento
    let h1_novo = document.createElement("h1");
    h1_novo.id = "elemento-faltam";
    h1_novo.textContent = "Faltam:";
    div_title.appendChild(h1_novo);

    let time_element = document.getElementById("time");
    time_element.hidden = false;

    setTimeout(() => {
        h1_novo.style.animationPlayState = "paused";

        let venue = document.getElementById("venue");
        if (venue != null) {
            venue.hidden = false;
        }
    }, 3000);
}, 40000);

// Exibe os botões para exibir o catálogo de presentes
setTimeout(() => {
    let botaoCatalogo = document.getElementById("botao-catalogo");
    botaoCatalogo.hidden = false;
}, 45000);

setTimeout(() => {
    let botaoEndereco = document.getElementById("botao-endereco");
    botaoEndereco.hidden = false;
}, 46000);

// Update the count down every 1 second
var x = setInterval(function () {
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("time").innerHTML =
        "<div class='container'><div class='days block'>" +
        days +
        "<br>Dias</div>" +
        "<div class='hours block'>" +
        hours +
        "<br>Horas</div>" +
        "<div class='minutes block'>" +
        minutes +
        "<br>Minutos</div>" +
        "<div class='seconds block'>" +
        seconds +
        "<br>Segundos</div></div>";

    // If the count down is over, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("time").innerHTML = "É hoje";
    }
}, 1000);

function handleClickCatalogo() {
    const giftLink =
        "https://lista-de-presentes-lucas-engen-6bb5f7989d978f6d07a72f2045b0fc95.gitlab.io/";

    window.open(giftLink);
}

function handleClickEndereco() {
    const addrLink =
        "https://www.universal.org/endereco/distrito-federal-sudoeste-15427/";

    window.open(addrLink);
}
